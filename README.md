# Baseball

Currently this just contains a database file with play-by-play information from the 2018 MLB season.

The columns in the table are a subset of those that can be found [here](http://chadwick.sourceforge.net/doc/cwevent.html) along with descriptions. The players are each given a unique ID based on their first/last name, which is described [here](https://www.retrosheet.org/retroID.htm) (along with a listing of all of them that is easy to search).

## DB Usage

`Retrosheet2018.db` is an sqlite database, and can be viewed by downloading [DB Browser for SQLite](https://sqlitebrowser.org/) (or any sqlite database software). Install the application, launch it, and then go to `File`->`Open Database` and select `Retrosheet2018.db`.

It will show you the table structure, and you can see the raw information underneath in the `Browse Data` tab. To get more interesting information however, we need to be able to query the table. These are run in the `Execute SQL` tab. There are some sample queries below.

#### Select all columns for 100 rows

`SELECT * FROM PlayByPlay2018 LIMIT 100`

#### The inning, count, and outs for a 100 plays where the Minnesota Twins were the away team

`SELECT INN_CT, BAT_HOME_ID, OUTS_CT, BALLS_CT, STRIKES_CT FROM PlayByPlay2018 WHERE AWAY_TEAM_ID='MIN' LIMIT 100`

#### All plays where Byron Buxton was on 2nd base and the Minnesota Twins were the away team

`SELECT * FROM PlayByPlay2018 WHERE AWAY_TEAM_ID='MIN' AND BAT_HOME_ID=0 AND BASE2_RUN_ID='buxtb001'`

#### 100 plays where the Minnesota Twins were the home team

`SELECT * FROM PlayByPlay2018 WHERE GAME_ID LIKE 'MIN%' LIMIT 100`

## Data Export

If you want the results of a query as a CSV, there is a save icon just to the bottom right of the results grid next to the text box that tells you how many results were returned. Click it to drop a list of your options down, one of which is `Export to CSV`.

## Data Disclaimer

The information used here was obtained free of
charge from and is copyrighted by Retrosheet.  Interested
parties may contact Retrosheet at 20 Sunset Rd.,
Newark, DE 19711.